/**
 * server.js
 * main / entry point for all things backend
 */

var express      = require("express")
  , path         = require("path")
  , logger       = require("morgan")
  , cookieParser = require("cookie-parser")
  , bodyParser   = require("body-parser")
  , compression  = require("compression") // optimization
  , app          = express();

var env = "development";

if (process.env.NODE_ENV) {
  env = process.env.NODE_ENV;
  console.log("NODE_ENV: ", env)
}

var config = require("./config/" + env + ".json");
app.config = config;

// @TODO
// this variable should be abstracted
// in case we want to introduce versioning
const baseAPIEndpoint = "/api";

// LOAD MIDDLEWARES
var cors   = require("./api/middlewares/cors");

// LOAD ENDPOINTS
var users = require("./api/endpoints/users");

// APPLY MIDDLEWARES
app.use(compression());
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors);
app.use(express.static("public"));

// APPLY ENDPOINTS
app.use(`${baseAPIEndpoint}/users`, users);

// SETUP STATIC FILE SERVING
app.get("*", (req, res) => {
  res.sendFile(__dirname + "/public/index.html");
});

app.listen(config.port, () => {
  console.log("server is running on port:", config.port);
});

module.exports = app;
