-- DB initialization

DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS posts CASCADE;
DROP TABLE IF EXISTS post_details CASCADE;

DROP TRIGGER IF EXISTS update_modified_column ON users;
DROP TRIGGER IF EXISTS update_modified_column ON posts;

CREATE OR REPLACE FUNCTION update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
  NEW.modified = now();
  RETURN NEW;
END;
$$ language 'plpgsql';

CREATE EXTENSION IF NOT EXISTS pgcrypto;
SELECT gen_random_uuid();

-- TABLE

CREATE TABLE IF NOT EXISTS users(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  name varchar(255) NOT NULL,
  username varchar(30) NOT NULL,
  password varchar NOT NULL,
  modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE IF NOT EXISTS posts(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  owner_id UUID,
  deleted_at TIMESTAMP WITH TIME ZONE,
  title varchar(80) NOT NULL,
  modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  FOREIGN KEY (owner_id) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS post_details(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  post_id UUID,
  details text,
  FOREIGN KEY (post_id) REFERENCES posts (id)
);

CREATE TRIGGER update_modified_column
BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE update_modified_column();

CREATE TRIGGER update_modified_column
BEFORE UPDATE ON posts FOR EACH ROW EXECUTE PROCEDURE update_modified_column();
