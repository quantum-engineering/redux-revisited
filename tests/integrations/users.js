/**
 * users.js
 *
 * [INTEGRATION TEST]
 */

var chai           = require("chai")
 , assert         = chai.assert
 , expect         = chai.expect
 , should         = chai.should()
 , chaiAsPromised = require("chai-as-promised")
 , chaiHttp       = require("chai-http");

chai.use(chaiAsPromised);
chai.use(chaiHttp);

var app          = requireLocal("server")
  , userFixtures = require("../fixtures/user");

describe("Users", () => {
  it("should be able to signup a new user", done => {
    chai.request(app)
      .post("/api/users/signup")
      .send(userFixtures.validSignupData)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      })
  });

  it("should be able to login existing users", done => {
    chai.request(app)
      .post("/api/users/auth")
      .send(userFixtures.validLoginData)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.data).to.include.keys("user_id", "token");
        done();
      })
  });

  it("should not allow signup for existing users measured by username", done => {
    chai.request(app)
      .post("/api/users/signup")
      .send(userFixtures.validSignupData)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        expect(res.body).to.include.keys("error");
        done();
      })
  });

})
