/**
 * validator.js
 *
 * [UNIT TEST]
 */

var chai           = require("chai")
  , assert         = chai.assert
  , expect         = chai.expect
  , should         = chai.should()
  , chaiAsPromised = require("chai-as-promised")
  , chaiHttp       = require("chai-http")
  , Promise        = require("bluebird");

chai.use(chaiAsPromised);
chai.use(chaiHttp);

var validator = requireLocal("api/helpers/validator");
var attributeFixtures = require("../fixtures/attributes");

describe("Validator", () => {
  it("should return a flattened / mapped object from inputed attributes", () => {
    var filteredData = validator(attributeFixtures.validArticleAttribute);
    return expect(filteredData).to.be.fulfilled
      .then(result => {
        var objCheck = {
          owner_id: 'f0130dc7-2532-4dd7-999d-9f8484b60973',
          title: 'Redux is awesome'
        };
        expect(result).to.deep.equal(objCheck);
      })
  });

  it("should throw an array of errors against bad data", () => {
    var filteredData = validator(attributeFixtures.emptyArticleAttribute);
    return expect(filteredData).to.be.rejected
      .then(result => {
        var arrayCheck = [ 'owner_id is a required field', 'title is a required field' ];
        expect(result.length).to.equal(2);
        expect(result).to.deep.equal(arrayCheck);
      })
  })
});
