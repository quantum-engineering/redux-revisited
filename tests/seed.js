/**
 * seed.js
 *
 * This is a seed script to populate the db with fake data
 * for unit and integration testing
 */

var Promise = require("bluebird")
  , faker   = require("faker")
  , app     = require("../server")
  , pg      = require("../api/adapters/db");

// Load models
var User = require("../api/models/User");

var tables = ["users", "posts", "post_details"];


// =============================
// TEARDOWN
// =============================

function tearDown() {
  return new Promise((resolve, reject) => {
    var bulkCreatePromise = [];
    tables.forEach(table => {
      console.log(`CLEARING ${table} TABLE`);
      bulkCreatePromise.push(pg(table).del());
    })

    resolve(Promise.all(bulkCreatePromise));
  })
}


// =============================
// SEED (WIP)
// =============================

tearDown()
  .then(() => { console.log("Shutting seeding process down"); process.exit() });
