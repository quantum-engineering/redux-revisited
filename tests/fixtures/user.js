/**
 * user.js
 *
 * [FIXTURES]
 */

module.exports = {
  validSignupData: {
    name: "gregory tandiono",
    username: "gregtandiono",
    password: "superawesomepassword2001"
  },
  invalidSignupData: {
    name: "gregory tandiono",
    username: "",
    password: "superawesomepassword2001"
  },

  validLoginData: {
    username: "gregtandiono",
    password: "superawesomepassword2001"
  },
  invalidLoginData: {
    username: "gtanjkkd",
    password: "thisisobviouslyaverybadpassword"
  }
};
