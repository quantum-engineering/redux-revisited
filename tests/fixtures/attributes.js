/**
 * attributes.js
 *
 * [FIXTURE]
 * model attributes
 */

module.exports = {
  emptyArticleAttribute: {
    owner_id: {value: "", type: "uuid", required: "true"},
    title: {value: "", type: "string", required: "true"}
  },

  validArticleAttribute: {
    owner_id: {value: "f0130dc7-2532-4dd7-999d-9f8484b60973", type: "uuid", required: "true"},
    title: {value: "Redux is awesome", required: "true"}
  },

  emptyUserAttribute: {
    name: {value: "", type: "string", required: "true"},
    username: {value: "", type: "string", required: "true"},
    password: {value: "", type: "string", required: "true"}
  }
}
